import { Car } from "./car.js";
import { Retangle } from "./retangle.js";
import { Square } from "./square.js";

var cars = [
    {
        name: "Toyota",
        year: 2019
    },
    {
        name: "Vinfast",
        year: 2020
    }
]

console.log("Car:");

// Khởi tạo các thực thể
var toyota = new Car("Toyota", 2019);
var vinFast = new Car("Vinfast", 2020);

console.log(toyota);
console.log(vinFast);
console.log(toyota.getName());
console.log(vinFast.getName());

console.log(toyota.getYear());
console.log(vinFast.getYear());

toyota.setYear(2020);
vinFast.setYear(2021);

console.log(toyota.getYear());
console.log(vinFast.getYear());

console.log(toyota.getAge());
console.log(vinFast.getAge());

console.log("Retangle:");

const retangle1 = new Retangle(10, 5);
const retangle2 = new Retangle(20, 5);

console.log(retangle1);
console.log(retangle2);
console.log(retangle1.getArea());
console.log(retangle2.getArea());

console.log("Square:");

const square1 = new Square(5);
const square2 = new Square(10, "Hình vuông to");

console.log(square1);
console.log(square2);
console.log(square1.getArea());
console.log(square2.getArea());
console.log(square1.getPerimeter());
console.log(square2.getPerimeter());

console.log(retangle1 instanceof Retangle); // T
console.log(retangle1 instanceof Square); // F
console.log(square1 instanceof Retangle); // T
console.log(square1 instanceof Square); // T

// Hoisting
a = a + 1;

var a; 